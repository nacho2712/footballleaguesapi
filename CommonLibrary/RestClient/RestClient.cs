﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace CommonLibrary.RestClient
{
    public class RestClient : IRestClient
    {
        private HttpClient _httpClient;

        public RestClient(string uri, IDictionary<string, string> headers = null, IList<string> accepts = null)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(uri)
            };

            // Add headers
            foreach (var header in headers)
            {
                client.DefaultRequestHeaders.Add(header.Key, header.Value);
            }

            // Add accepts
            client.DefaultRequestHeaders.Accept.Clear();
            foreach (var accept in accepts)
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(accept));
            }

            _httpClient = client;
        }

        public MyObject GetT<MyObject>(string requestUri) where MyObject : class
        {
            var response = _httpClient.GetAsync(requestUri);

            var message = response.Result;
            if (!message.IsSuccessStatusCode && message.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                return null;
            }

            var respObj = message.Content.ReadAsAsync<MyObject>().Result;

            return respObj;
        }

        public string Post(string url, string body)
        {
            StringContent queryString = new StringContent(body);
            var response = _httpClient.PostAsJsonAsync(url, queryString);

            var respObj = response.Result.Content.ReadAsStringAsync().Result;
            return respObj;
        }
    }
}
