﻿
namespace CommonLibrary.RestClient
{
    public interface IRestClient
    {
        MyObject GetT<MyObject>(string url) where MyObject : class;

        string Post(string url, string body);
    }
}
