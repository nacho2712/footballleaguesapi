﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    [Table("player")]
    public class PlayerEntity : IEntity
    {
        public string Name { get; set; }

        public string Position { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string CountryOfBirth { get; set; }

        public string Nationality { get; set; }

        public short TeamID { get; set; }

        [ForeignKey("TeamID")]
        public virtual TeamEntity Team { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public short ID { get; set; }

        public object GetId()
        {
            return ID;
        }
    }
}
