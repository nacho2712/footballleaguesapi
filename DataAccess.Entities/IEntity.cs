﻿using System;

namespace DataAccess.Entities
{
	public interface IEntity
	{
		object GetId();
	}
}
