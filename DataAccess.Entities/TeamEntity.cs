﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    [Table("team")]
    public class TeamEntity : IEntity
    {
        public string Name { get; set; }

        public string Tla { get; set; }

        public string ShortName { get; set; }

        public string AreaName { get; set; }

        public string Email { get; set; }

        public short CompetitionID { get; set; }

        [ForeignKey("CompetitionID")]
        public virtual CompetitionEntity Competition { get; set; }

        public virtual List<PlayerEntity> Players { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public short ID { get; set; }

        public object GetId()
        {
            return ID;
        }
    }
}
