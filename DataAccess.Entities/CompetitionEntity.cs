﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    [Table("competition")]
    public class CompetitionEntity : IEntity
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string AreaName { get; set; }

        public virtual List<TeamEntity> Teams { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public short ID { get; set; }

        public object GetId()
        {
            return ID;
        }
    }
}
