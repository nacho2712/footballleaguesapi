﻿using DataAccess.Entities;
using DataAccess.Repository;
using System;
using System.Collections.Generic;

namespace HME.Timer.Testing.Shared
{
    public static class DataMock
    {
        #region UT utils data mock related
        public static void CleanUpRepo(IRepository repo)
        {
            repo.CompetitionRepository.DeleteAll();
            repo.TeamRepository.DeleteAll();
            repo.PlayerRepository.DeleteAll();
        }

        public static void CreateMockData(IRepository repo)
        {
            var competitions = new List<CompetitionEntity>()
            {
                new CompetitionEntity
                {
                    ID = 1,
                    AreaName = "Europe",
                    Code = "CL",
                    Name = "Champions League"
                },
                new CompetitionEntity
                {
                    ID = 2,
                    AreaName = "America",
                    Code = "AC",
                    Name = "American Cup"
                },
                new CompetitionEntity
                {
                    ID = 3,
                    AreaName = "America",
                    Code = "SL",
                    Name = "Super Liga"
                }
            };
            repo.CompetitionRepository.AddAll(competitions);

            var teams = new List<TeamEntity>()
            {
                new TeamEntity
                {
                    ID = 1,
                    CompetitionID = 1,
                    AreaName = "Europe",
                    Name = "Barcelona",
                    ShortName = "Barca"
                },
                new TeamEntity
                {
                    ID = 2,
                    CompetitionID = 1,
                    AreaName = "Europe",
                    Name = "Real Madrid",
                    ShortName = "Real"
                },
                new TeamEntity
                {
                    ID = 3,
                    CompetitionID = 2,
                    AreaName = "America",
                    Name = "Brasil",
                    ShortName = "Real"
                },
                new TeamEntity
                {
                    ID = 4,
                    CompetitionID = 2,
                    AreaName = "America",
                    Name = "Argentina"
                },
                new TeamEntity
                {
                    ID = 5,
                    CompetitionID = 3,
                    AreaName = "America",
                    Name = "Boca",
                    ShortName = "Xeneize"
                },
                new TeamEntity
                {
                    ID = 6,
                    CompetitionID = 3,
                    AreaName = "America",
                    Name = "River",
                    ShortName = "Millonario"
                },
            };
            repo.TeamRepository.AddAll(teams);

            var players = new List<PlayerEntity>
            {
                new PlayerEntity
                {
                    ID = 1,
                    TeamID = 1,
                    CountryOfBirth = "ARG",
                    Name = "Lionel Messi",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 2,
                    TeamID = 1,
                    CountryOfBirth = "URG",
                    Name = "Luis Suarez",
                    Nationality = "Uruguayan",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 3,
                    TeamID = 2,
                    CountryOfBirth = "POR",
                    Name = "Cristiano Ronaldo",
                    Nationality = "Portuguese",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 4,
                    TeamID = 2,
                    CountryOfBirth = "SPN",
                    Name = "Sergio Ramos",
                    Nationality = "Spanish",
                    Position = "Defender",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 5,
                    TeamID = 3,
                    CountryOfBirth = "BRA",
                    Name = "Neymar",
                    Nationality = "Brasilean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 6,
                    TeamID = 3,
                    CountryOfBirth = "BRA",
                    Name = "Coutinho",
                    Nationality = "Brasilean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 7,
                    TeamID = 4,
                    CountryOfBirth = "ARG",
                    Name = "Lionel Messi",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 8,
                    TeamID = 4,
                    CountryOfBirth = "ARG",
                    Name = "Aguero",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 9,
                    TeamID = 5,
                    CountryOfBirth = "ARG",
                    Name = "Abila",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 10,
                    TeamID = 5,
                    CountryOfBirth = "ARG",
                    Name = "Zárate",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 11,
                    TeamID = 5,
                    CountryOfBirth = "ARG",
                    Name = "Zárate",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 12,
                    TeamID = 6,
                    CountryOfBirth = "ARG",
                    Name = "Fernandez",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
                new PlayerEntity
                {
                    ID = 13,
                    TeamID = 6,
                    CountryOfBirth = "ARG",
                    Name = "Pratto",
                    Nationality = "Argentinean",
                    Position = "Forward",
                    DateOfBirth = DateTime.Now
                },
            };
            repo.PlayerRepository.AddAll(players);

            repo.Save();
        }        

        #endregion UT utils data mock related
    }
}