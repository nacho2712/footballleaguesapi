using DataAccess.Core;
using DataAccess.Entities;
using DataAccess.Repository;
using HME.Timer.Testing.Shared;
using Microsoft.EntityFrameworkCore;
using Moq;
//using NUnit.Framework;
using System.Linq;
using Xunit;
using Assert = NUnit.Framework.Assert;

namespace DataAccessTests
{
    
    public class DataAccessTests
    {
        private DbContextOptions<FootballLeagueContext> options;
        private Mock<IFootballLeagueRepoFactory> repoFactoryMock;
        private Mock<FootballLeagueContext> footballLeagueMock;

        private FootballLeagueContext getNewContext(DbContextOptions<FootballLeagueContext> options)
        {
            return new FootballLeagueContext(options);
        }

        public DataAccessTests()
        {
            options = new DbContextOptionsBuilder<FootballLeagueContext>()
                .UseInMemoryDatabase(databaseName: "sys60InMem")
                .EnableSensitiveDataLogging(true)
                .UseLazyLoadingProxies()
                .Options;

            footballLeagueMock = new Mock<FootballLeagueContext>();

            repoFactoryMock = new Mock<IFootballLeagueRepoFactory>();
            repoFactoryMock.Setup(x => x.GetNewRepository(footballLeagueMock.Object)).Returns(() => new Repository(getNewContext(options)));

            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                DataMock.CleanUpRepo(repository);
                DataMock.CreateMockData(repository);
            }
        }

        [Fact]
        public void TestRepositoryIsValid()
        {
            using (var repo = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                Assert.NotNull(repo.CompetitionRepository, "Competition Repository must be initialized");
                Assert.NotNull(repo.TeamRepository, "Team Repository must be initialized");
                Assert.NotNull(repo.PlayerRepository, "Player Repository must be initialized");
            }
        }

        [Fact]
        public void TestRemoveCompetition()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                short id = 1;
                repository.CompetitionRepository.Delete(id);
                repository.Save();

                Assert.IsNull(repository.CompetitionRepository.DbSet.FirstOrDefault(e => e.ID == 1));
            }
        }

        [Fact]
        public void TestRemoveTeam()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                short id = 1;
                repository.TeamRepository.Delete(id);
                repository.Save();

                Assert.IsNull(repository.TeamRepository.DbSet.FirstOrDefault(e => e.ID == 1));
            }
        }

        [Fact]
        public void TestRemoveLaneLayout()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                short id = 1;
                repository.PlayerRepository.Delete(id);
                repository.Save();

                Assert.IsNull(repository.PlayerRepository.DbSet.FirstOrDefault(e => e.ID == 1));
            }
        }

        [Fact]
        public void TestUpdateCompetition()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                var data = new CompetitionEntity
                {
                    ID = 1,
                    Code = "CL",
                    Name = "Liga de campeones"
                };
                repository.CompetitionRepository.Update(data);
                repository.Save();

                Assert.IsNotNull(repository.CompetitionRepository.DbSet.
                    FirstOrDefault
                    (e =>
                        e.ID == data.ID && 
                        e.Code == data.Code &&
                        e.Name == data.Name
                    ));
            }
        }

        [Fact]
        public void TestUpdateTeam()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                var data = new TeamEntity
                {
                    ID = 1,
                    Name = "El Barca"
                };
                repository.TeamRepository.Update(data);
                repository.Save();

                Assert.IsNotNull(repository.TeamRepository.DbSet.
                    FirstOrDefault
                    (e => 
                        e.ID == data.ID && 
                        e.Name == data.Name
                    ));
            }
        }

        [Fact]
        public void TestUpdatePlayer()
        {
            using (var repository = repoFactoryMock.Object.GetNewRepository(footballLeagueMock.Object))
            {
                var data = new PlayerEntity
                {
                    ID = 1,
                    Name = "Messi Lio"
                };
                repository.PlayerRepository.Update(data);
                repository.Save();

                Assert.IsNotNull(repository.PlayerRepository.DbSet.
                    FirstOrDefault(e => e.ID == data.ID
                    && e.Name == data.Name));
            }
        }
    }
}
