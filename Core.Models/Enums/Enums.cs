namespace Enums
{
    public enum ProcessStatus
    {
        Success,
        Fail
    }

    public enum ImportStatus
    {
        Unknow,
        Successfully,
        AlreadyImported,
        NotFound,
        ServerError
    }
}