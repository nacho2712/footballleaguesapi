
namespace Classes
{
    public class Team
    {
        public Team(string name, string tla, string shortName, string areaName, string email)
        {
            this.Name = name;
            this.Tla = tla;
            this.ShortName = shortName;
            this.AreaName = areaName;
            this.Email = email;
        }

        public string Name { get; private set; }

        public string Tla { get; private set; }

        public string ShortName { get; private set; }

        public string AreaName { get; private set; }

        public string Email { get; private set; }
    }
}