using System;

namespace Classes
{
    public class Player
    {
        public Player(string name, string position, DateTime dateOfBirth, string countryOfBirth, string nationatily)
        {
            this.Name = name;
            this.Position = position;
            this.DateOfBirth = dateOfBirth;
            this.CountryOfBirth = countryOfBirth;
            this.Nationatily = nationatily;
        }

        public string Name { get; private set; }

        public string Position { get; private set; }

        public DateTime DateOfBirth { get; private set; }

        public string CountryOfBirth { get; private set; }

        public string Nationatily { get; private set; }

        // public Team Team { get; private set; }
    }
}