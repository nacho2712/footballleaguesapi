
namespace Classes
{
    public class Competition
    {
        public Competition(string name, string code, string areaName)
        {
            this.Name = name;
            this.Code = code;
            this.AreaName = areaName;
        }

        public string Name { get; private set; }

        public string Code { get; private set; }

        public string AreaName { get; private set; }
    }
}