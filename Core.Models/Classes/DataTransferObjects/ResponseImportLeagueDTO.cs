using Enums;
using System.Runtime.Serialization;

namespace Classes
{
    [DataContract]
    public class ResponseImportLeagueDTO
    {
        [DataMember]
        public string message { get; set; }
    }
}