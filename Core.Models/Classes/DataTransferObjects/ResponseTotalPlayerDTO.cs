using System.Runtime.Serialization;

namespace Classes
{
    [DataContract]
    public class ResponseTotalPlayerDTO
    {
        [DataMember]
        public int total { get; set; }
    }
}