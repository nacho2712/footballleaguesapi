﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace DataAccess.Core
{
	public class BaseDbContext : DbContext
	{
        public string ConnectionString { get; set; } = Path.Combine(Path.GetFullPath("../DataAccess.Core"), "ConfigurationFiles");


        public BaseDbContext(DbContextOptions options) : base(options)
		{

		}

		protected BaseDbContext()
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				try
				{
                    var configuration = new ConfigurationBuilder()
                        .SetBasePath(Path.Combine(Path.GetFullPath("../DataAccess.Core"), "ConfigurationFiles"))
                        .AddJsonFile("DataAccess.Config.json")
                        .Build();

                    var connectionString = configuration.GetValue<string>("ConnectionString");

                    optionsBuilder.UseLazyLoadingProxies().UseMySQL
                    (connectionString, opts => {
                        opts.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds);
                        opts.MigrationsHistoryTable("__EFMigrationsHistory");
                    });
				}
				catch (Exception ex)
				{ }
			}
		}

	}
}
