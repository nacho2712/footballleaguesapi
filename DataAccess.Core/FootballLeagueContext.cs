﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace DataAccess.Core
{
	public class FootballLeagueContext : BaseDbContext
	{
		public FootballLeagueContext()
		{
		}

		public FootballLeagueContext(string connString)
		{
			ConnectionString = connString;
		}

        public FootballLeagueContext(DbContextOptions<FootballLeagueContext> options) : base(options)
        { }

		/// <summary>
		/// Enum auxiliary action to convet to string
		/// </summary>
		/// <typeparam name="T"> T must be an enum type</typeparam>
		/// <returns>string value of enum T</returns>
		private Expression<Func<T, string>> ToString<T>()
		{
			return v => v.ToString();
		}

		/// <summary>
		/// Enum auxiliary action to convert from string
		/// </summary>
		/// <typeparam name="T">T must be an enum type</typeparam>
		/// <returns>Enum value of string</returns>
		private Expression<Func<string, T>> Conversion<T>()
		{
			return v => (T)Enum.Parse(typeof(T), v);

		}

		private string EnumToColumnType(Type enumType)
		{
			return string.Format(
				"enum({0})",
				string.Join(',', Enum.GetNames(enumType).Select(x => string.Format("'{0}'", x)))
			);
		}

		/// <summary>
		/// This is meant to set the ef core entities properties and relationships between each other 
		/// </summary>
		/// <param name="modelBuilder"></param>
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CompetitionEntity>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(f => f.ID).ValueGeneratedOnAdd();
                entity.Property(f => f.Name);
                entity.Property(e => e.AreaName);
                entity.Property(e => e.Code);

                entity.HasMany(b => b.Teams).WithOne();
            });

            modelBuilder.Entity<TeamEntity>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(f => f.ID).ValueGeneratedOnAdd();
                entity.Property(f => f.Name);
                entity.Property(e => e.Tla);
                entity.Property(e => e.ShortName);
                entity.Property(e => e.AreaName);
                entity.Property(e => e.Email);
                entity.Property(e => e.CompetitionID);

                entity
                   .HasOne(p => p.Competition)
                   .WithMany(b => b.Teams)
                   .HasForeignKey(e => e.CompetitionID);

                entity.HasMany(b => b.Players).WithOne();
            });

            modelBuilder.Entity<PlayerEntity>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(f => f.ID).ValueGeneratedOnAdd();
                entity.Property(f => f.Name);
                entity.Property(f => f.Position);
                entity.Property(f => f.CountryOfBirth);
                entity.Property(f => f.Nationality);
                entity.Property(f => f.DateOfBirth);

                entity.Property(e => e.TeamID);

                entity
                   .HasOne(p => p.Team)
                   .WithMany(b => b.Players)
                   .HasForeignKey(e => e.TeamID);
            });
        }
	}
}
