﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Core.Migrations
{
    public partial class IntialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "competition",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    AreaName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_competition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "team",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Tla = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    AreaName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    CompetitionID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_team", x => x.ID);
                    table.ForeignKey(
                        name: "FK_team_competition_CompetitionID",
                        column: x => x.CompetitionID,
                        principalTable: "competition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "player",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    CountryOfBirth = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(nullable: true),
                    TeamID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_player", x => x.ID);
                    table.ForeignKey(
                        name: "FK_player_team_TeamID",
                        column: x => x.TeamID,
                        principalTable: "team",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_player_TeamID",
                table: "player",
                column: "TeamID");

            migrationBuilder.CreateIndex(
                name: "IX_team_CompetitionID",
                table: "team",
                column: "CompetitionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "player");

            migrationBuilder.DropTable(
                name: "team");

            migrationBuilder.DropTable(
                name: "competition");
        }
    }
}
