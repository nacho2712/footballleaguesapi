﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace DataAccess.Core
{
	public class FootballLeagueContextFactory : IDesignTimeDbContextFactory<FootballLeagueContext>
	{
		public FootballLeagueContext CreateDbContext(string[] args)
		{

			var configuration = new ConfigurationBuilder()
				.SetBasePath(Path.Combine(Path.GetFullPath("../DataAccess.Core"), "ConfigurationFiles"))
				.AddJsonFile("DataAccess.Config.json")				
				.Build();

			var dbContextBuilder = new DbContextOptionsBuilder<FootballLeagueContext>();

			var connectionString = configuration.GetValue<string>("ConnectionString");

			dbContextBuilder.UseLazyLoadingProxies().UseMySQL
				(connectionString, opts => {
					opts.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds);
					opts.MigrationsHistoryTable("__EFMigrationsHistory");
				});

			return new FootballLeagueContext(dbContextBuilder.Options);
		}
	}
}