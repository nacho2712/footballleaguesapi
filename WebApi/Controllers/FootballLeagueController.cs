using Classes;
using Core;
using Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Aplication.Controllers
{
    [ApiController]
    public class FootballLeagueController : ControllerBase
    {
        private IServiceManager _serviceManager;

        public FootballLeagueController(IServiceManager serviceManger)
        {
            _serviceManager = serviceManger;
        }


        [HttpGet("import-league/{leagueCode}")]
        public ObjectResult ImportLeague(string leagueCode)
        {
            ImportStatus importStatus;
            try
            {
                importStatus = _serviceManager.ImportLeague(leagueCode);
            }
            catch (Exception)
            {
                importStatus = ImportStatus.ServerError;
            }

            var (statusCode, message) = GetStatusCodeAndMessage(importStatus);
            var messageDTO = new ResponseImportLeagueDTO() { message = message };

            return StatusCode(statusCode, messageDTO);
        }

        [HttpGet("total-players/{leagueCode}")]
        public ObjectResult GetTotalPlayersByLeague(string leagueCode)
        {
            try
            {
                if(_serviceManager.TryGetTotalPlayersNumberByLeagueCode(leagueCode, out var numberOfPlayer))
                {
                    var messageDTO = new ResponseTotalPlayerDTO()
                    {
                        total = numberOfPlayer
                    };
                    return Ok(messageDTO);
                }
                else
                {
                    var (_, message) = GetStatusCodeAndMessage(ImportStatus.NotFound);
                    return NotFound(new ResponseImportLeagueDTO() { message = message });
                }
            }
            catch (Exception)
            {
                var (statusCode, message) = GetStatusCodeAndMessage(ImportStatus.ServerError);
                return StatusCode(statusCode, new ResponseImportLeagueDTO() { message = message });
            }
        }


        private (int statusCode, string message) GetStatusCodeAndMessage(ImportStatus importStatus)
        {
            var statusCode = -1;
            var message = string.Empty;
            switch (importStatus)
            {
                case ImportStatus.Successfully:
                    message = "Successfully imported";
                    statusCode = 200;
                    break;

                case ImportStatus.AlreadyImported:
                    message = "League already imported";
                    statusCode = 409;
                    break;

                case ImportStatus.NotFound:
                    message = "League Not found";
                    statusCode = 404;
                    break;

                case ImportStatus.ServerError:
                    message = "Server Error";
                    statusCode = 504;
                    break;
            }

            return (statusCode, message);
        }
    }
}
