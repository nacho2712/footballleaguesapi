﻿using DataAccess.Core;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DataAccess.Repository
{

    //Generic Repository for the a specific entity
    public class GenericRepository<T> where T: class , IEntity 
	{
		public virtual DbContext Context { get; set; }
		public virtual DbSet<T> DbSet { get; set; }


		public GenericRepository(FootballLeagueContext context = null)
		{
			this.Context = context;
			DbSet = context.Set<T>();

		}

		public GenericRepository(FootballLeagueContext context, DbSet<T> set)
		{
			this.Context = context;
			DbSet = set;
		}

		public virtual T GetByID(object id)
		{
			return DbSet?.Find(id);
		}

		public virtual void Add(T entity)
		{
			DbSet?.Add(entity);
		}

		public virtual void AddAll(List<T> entities)
		{
			DbSet?.AddRange(entities);
		}

		public virtual void Delete(object id)
		{
			T entityToDelete = DbSet?.Find(id);
			Delete(entityToDelete);
		}

		public virtual void Delete(T entity)
		{
			DbSet?.Remove(entity);
		}

		public virtual void Update(T entity)
		{
			DbSet?.Update(entity);
			if (Context != null)
			{
				var e = Context.Entry(entity);
				if (e != null) { 
					e.State = EntityState.Modified;
				}
			}
			
		}

		public virtual void DeleteAll()
		{
			foreach (var e in DbSet)
			{
				Context.Entry(e).State = EntityState.Deleted;
			}
		}


	}
}
