﻿
using DataAccess.Core;
using System;

namespace DataAccess.Repository
{
	public interface IFootballLeagueRepoFactory
	{

		IRepository GetNewRepository();

        IRepository GetNewRepository(FootballLeagueContext janusContext);
    }
		
}
