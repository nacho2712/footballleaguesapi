﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataAccess.Repository
{
	public interface IRepository : IDisposable
	{
        GenericRepository<CompetitionEntity> CompetitionRepository { get; set; }
        GenericRepository<TeamEntity> TeamRepository { get; set; }
        GenericRepository<PlayerEntity> PlayerRepository { get; set; }

        void Save();
	}
}
