﻿using DataAccess.Core;
using DataAccess.Entities;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DataAccessTests")]
namespace DataAccess.Repository
{
    internal class Repository : IRepository
	{
		private readonly FootballLeagueContext context;

        private GenericRepository<CompetitionEntity> competitionRepository;
        private GenericRepository<TeamEntity> teamRepository;
        private GenericRepository<PlayerEntity> playerRepository;

        public Repository(FootballLeagueContext context)
		{
			this.context = context;
		}

        public virtual GenericRepository<CompetitionEntity> CompetitionRepository
        {
            get
            {
                GetNewGenericRepo(ref competitionRepository);
                return competitionRepository;
            }

            set { competitionRepository = value; }
        }

        public virtual GenericRepository<TeamEntity> TeamRepository
        {
            get
            {
                GetNewGenericRepo(ref teamRepository);
                return teamRepository;
            }

            set { teamRepository = value; }
        }

        public virtual GenericRepository<PlayerEntity> PlayerRepository
        {
            get
            {
                GetNewGenericRepo(ref playerRepository);
                return playerRepository;
            }

            set { playerRepository = value; }
        }

        private void GetNewGenericRepo<T>(ref GenericRepository<T> repo) where T : class, IEntity
		{
			if (repo == null)
			{
				repo = new GenericRepository<T>(context);
			}
		}

		public void Save()
		{
			context.SaveChanges();
		}

		public void Dispose()
		{
			context.Dispose();
			GC.SuppressFinalize(this);
		}

	}
}