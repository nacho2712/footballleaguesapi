﻿using DataAccess.Core;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
	public class FootballLeagueRepoFactory : IFootballLeagueRepoFactory
	{
		private readonly string _connectionString;
		private readonly DbContextOptions<FootballLeagueContext> _options;

		public FootballLeagueRepoFactory()
		{

		}

		public FootballLeagueRepoFactory(string connectionString)
		{
			_connectionString = connectionString;
		}

		public FootballLeagueRepoFactory(DbContextOptions<FootballLeagueContext> options)
		{
			_options = options;
		}

		private FootballLeagueContext GetNewFootballLeagueContext()
		{
			if (_options != null)
			{
				return new FootballLeagueContext(_options);
			}
			else
			{
				if (_connectionString != null)
				{
					return new FootballLeagueContext(_connectionString);
				}
				else return new FootballLeagueContext();
			}
		}

		public IRepository GetNewRepository()
		{
			return new Repository(GetNewFootballLeagueContext());
		}

        public IRepository GetNewRepository(FootballLeagueContext janusContext)
        {
            return new Repository(janusContext);
        }
    }
}
