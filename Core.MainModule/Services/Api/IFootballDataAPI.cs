﻿namespace Core.Services
{
    public interface IFootballDataAPI
    {
        bool TryGetCompetitionDataByCode(string code, out CompetitionTeamsData competition);
        bool TryGetTeamDataByCode(int code, out TeamData teamData);
    }
}
