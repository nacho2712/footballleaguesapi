﻿
using CommonLibrary.RestClient;
using System.Collections.Generic;

namespace Core.Services
{
    public class FootballDataAPI : IFootballDataAPI
    {
        #region Private properties
        private IRestClient Client { get; }

        private readonly string url = "https://api.football-data.org/v2/";
        private readonly IList<string> accepts = new List<string>() { "application/json" };
        
        // Replace value by the current token.
        private readonly IDictionary<string, string> headers = new Dictionary<string, string>() { 
                                                            { "X-Auth-Token", "211faf0f2b5c40e689876ede30391e94" } };
        #endregion

        #region Constructor
        public FootballDataAPI()
        {
            Client = new RestClient(url, this.headers, this.accepts);
        }
        #endregion

        const string _competitionResourceName = "competitions";
        const string _teamResourceName = "teams";

        public bool TryGetCompetitionDataByCode(string code, out CompetitionTeamsData competitionData)
        {
            var requestUri = $"{_competitionResourceName}/{code}/teams";
            competitionData = Client.GetT<CompetitionTeamsData>(requestUri);

            return competitionData != null;
        }

        public bool TryGetTeamDataByCode(int code, out TeamData teamData)
        {
            var requestUri = $"{_teamResourceName}/{code}";
            teamData = Client.GetT<TeamData>(requestUri);

            return teamData != null;
        }
    }

}
