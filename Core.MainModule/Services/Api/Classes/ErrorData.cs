﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services
{
    public class ErrorData
    {
        public string message { get; set; }
        public int errorCode { get; set; }
    }

}
