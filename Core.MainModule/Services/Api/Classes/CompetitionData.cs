﻿using System;

namespace Core.Services
{
    public class CompetitionData
    {
        public int id { get; set; }
        public Area area { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public object emblemUrl { get; set; }
        public string plan { get; set; }
        public Currentseason currentSeason { get; set; }
        public Season[] seasons { get; set; }
        public DateTime lastUpdated { get; set; }
    }
    

    public class Season
    {
        public int id { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int? currentMatchday { get; set; }
        public Winner winner { get; set; }
    }
}