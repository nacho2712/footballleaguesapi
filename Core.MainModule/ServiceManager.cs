using Core.Services;
using DataAccess.Entities;
using DataAccess.Repository;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Core
{
    public class ServiceManager : IServiceManager
    {
        private IFootballDataAPI _footballDataAPI;
        private IRepository _repository;

        public ServiceManager(IFootballLeagueRepoFactory repository, IFootballDataAPI footballDataAPI)
        {
            _repository = repository.GetNewRepository();
            _footballDataAPI = footballDataAPI;
        }

        public ImportStatus ImportLeague(string leagueCode)
        {
            ImportStatus importStatus;

            // Validate if the league already was imported
            if (TryGetCompetitionByCode(leagueCode, out var competitionInDataBase))
            {
                return ImportStatus.AlreadyImported;
            }
            else
            {
                // If not was imported, then try to get the league from the API

                // If not found the league by the code, return NotFound.
                CompetitionTeamsData competitionData = null;
                if (!_footballDataAPI.TryGetCompetitionDataByCode(leagueCode, out competitionData))
                {
                    return ImportStatus.NotFound;
                }

                // Hidrate CompetitionEntity base.
                var competitionEntity = new CompetitionEntity
                {
                    ID = (short)competitionData.competition.id,
                    Name = competitionData.competition.name,
                    Code = competitionData.competition.code,
                    AreaName = competitionData.competition.area.name
                };

                _repository.CompetitionRepository.DbSet.Add(competitionEntity);

                // Hidrate Team table with by competition.
                var requestCount = 0;
                foreach (var team in competitionData.teams)
                {
                    var teamEntity = new TeamEntity
                    {
                        CompetitionID = competitionEntity.ID,
                        ID = (short)team.id,
                        AreaName = team.area.name,
                        Email = team.email,
                        Name = team.name,
                        ShortName = team.shortName,
                        Tla = team.tla
                    };

                    _repository.TeamRepository.DbSet.Add(teamEntity);

                    // Hidrate PlayerTable by team
                    // I put ServerError  because I think that if I can get competetion data, so I could get team's data correctly
                    if(!_footballDataAPI.TryGetTeamDataByCode(team.id, out var teamData) && teamData?.squad != null)
                    {
                        importStatus = ImportStatus.ServerError;
                    }

                    requestCount++;
                    foreach (var player in teamData.squad)
                    {
                        var playerEntity = new PlayerEntity
                        {
                            Position = player.position,
                            CountryOfBirth = player.countryOfBirth,
                            DateOfBirth = player.dateOfBirth,
                            Name = player.name,
                            Nationality = player.nationality,
                            TeamID = teamEntity.ID
                        };

                        _repository.PlayerRepository.DbSet.Add(playerEntity);
                    }

                    // It's to avoid the block from API service.
                    if(requestCount == 10)
                    {
                        requestCount = 0;
                        Thread.Sleep((int)TimeSpan.FromMinutes(1).TotalMilliseconds);
                    }
                }

                _repository.Save();

               importStatus = ImportStatus.Successfully;
            }

            return importStatus;
        }

        public bool TryGetTotalPlayersNumberByLeagueCode(string leagueCode, out int numberOfPlayers)
        {
            leagueCode = leagueCode.ToUpper();
            if (TryGetCompetitionByCode(leagueCode, out CompetitionEntity competition))
            {
                var teams = _repository.TeamRepository.DbSet.Where(x => x.CompetitionID == competition.ID);
                numberOfPlayers = teams.Sum(x => x.Players.Count());
                return true;
            }
            
            numberOfPlayers = -1;
            return false;            
        }


        #region private methods

        private bool TryGetCompetitionByCode(string leagueCode, out CompetitionEntity competition)
        {
            competition = _repository.CompetitionRepository.DbSet.FirstOrDefault(x => x.Code == leagueCode);

            return competition != null;
        }

        #endregion
    }
}