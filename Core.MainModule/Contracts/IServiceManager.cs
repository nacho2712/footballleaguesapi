using Enums;

namespace Core
{
    public interface IServiceManager
    {
        ImportStatus ImportLeague(string leagueCode);

        bool TryGetTotalPlayersNumberByLeagueCode(string leagueCode, out int numberOfPlayers);
    }
}