
using CommonLibrary;
using DataAccess.Entities;
using DataAccess.Repository;
using System.Collections.Generic;
using System.Linq;

public class ServiceManager : IServiceManager
{
    private IFootballDataService _footballDataService;
    private IRepository _repository;

    public ServiceManager(IFootballLeagueRepoFactory repository, IFootballDataService footballDataService)
    {
        _repository = repository.GetNewRepository();
        _footballDataService = footballDataService;
    }

    public ImportStatus ImportLeague(string leagueCode)
    {
        ImportStatus importStatus;
        if (ValidateIfLeagueIsInDatabaseByCode(leagueCode))
        {
            importStatus = ImportStatus.AlreadyImported;
        }
        else
        {
            var competitionData = _footballDataService.GetCompetitionDataByCode(leagueCode);

            // Hidrate CompetitionEntity base.
            var competitionEntity = new CompetitionEntity();
            competitionEntity.ID = (short)competitionData.competition.id;
            competitionEntity.Name = competitionData.competition.name;
            competitionEntity.Code = competitionData.competition.code;
            competitionEntity.AreaName = competitionData.competition.area.name;

            // Hidrate CompetitionEntity with their teams.
            var teamsEntities = new List<TeamEntity>();
            foreach (var team in competitionData.teams)
            {
                var teamEntity = new TeamEntity
                {
                    CompetitionID = (short)competitionData.competition.id,
                    ID = (short)team.id,
                    AreaName = team.area.name,
                    Email = team.email,
                    Name = team.name,
                    ShortName = team.shortName,
                    Tla = team.tla
                };

                // Hidrate Teams with their players
                var playerEntities = new List<PlayerEntity>();
                var players = _footballDataService.GetPlayersByTeamCode(team.id);
                foreach (var player in players)
                {
                    playerEntities.Add(new PlayerEntity
                    {
                        Position = player.position,
                        CountryOfBirth = player.countryOfBirth,
                        DateOfBirth = player.dateOfBirth,
                        Name = player.name,
                        Nationality = player.nationality,
                        TeamID = (short)team.id
                    });
                }
                teamEntity.Players = playerEntities;

                teamsEntities.Add(teamEntity);
            }

            competitionEntity.Teams = teamsEntities;

            importStatus = ImportStatus.Successfully;
        }

        return importStatus;
    }

    public int GetTotalPlayersNumberByLeagueCode(string leagueCode)
    {
        return 19;
    }


    #region private methods

    private bool ValidateIfLeagueIsInDatabaseByCode(string leagueCode)
    {
        var league = _repository.CompetitionRepository.DbSet.FirstOrDefault(x => x.Code == leagueCode);

        return league != null;
    }

    #endregion
}